class Roman
  def self.parse num
    return parse_ones num if num < 10
    nums = num.to_s.split("").map{|i| i.to_i}
    return parse_tens(nums[0]) + parse(nums.drop(1).join.to_i) if num < 100
    return parse_hundreds(nums[0]) + parse(nums.drop(1).join.to_i) if num < 1000
  end

  def self.parse_ones num
    [:error, "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"][num]
  end

  def self.parse_tens num
    [:error, "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"][num]
  end

  def self.parse_hundreds num
    [:error, "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"][num]
  end
end
