require_relative '../lib/roman.rb'
require 'minitest/autorun'

class RomanTest < Minitest::Unit::TestCase
  def test_one
    assert_equal "I", Roman.parse(1)
  end
  def test_two
    assert_equal "II", Roman.parse(2)
  end
  def test_three
    assert_equal "III", Roman.parse(3)
  end
  def test_four
    assert_equal "IV", Roman.parse(4)
  end
  def test_five
    assert_equal "V", Roman.parse(5)
  end
  def test_six
    assert_equal "VI", Roman.parse(6)
  end
  def test_seven
    assert_equal "VII", Roman.parse(7)
  end
  def test_eight
    assert_equal "VIII", Roman.parse(8)
  end
  def test_nine
    assert_equal "IX", Roman.parse(9)
  end
  def test_twenty_three
    assert_equal "XXIII", Roman.parse(23)
  end
  def test_nine_fourteen
    assert_equal "CMXIV", Roman.parse(914)
  end
  def test_four_eighty_eight
    assert_equal "CDLXXXVIII", Roman.parse(488)
  end
end

