defmodule RomanTest do
  use ExUnit.Case

  test "1 returns 'I'", do: assert Roman.parse(1) == "I"
  test "2 returns 'II'", do: assert Roman.parse(2) == "II"
  test "3 returns 'III'", do: assert Roman.parse(3) == "III"
  test "4 returns 'IV'", do: assert Roman.parse(4) == "IV"
  test "5 returns 'V'", do: assert Roman.parse(5) == "V"
  test "6 returns 'VI'", do: assert Roman.parse(6) == "VI"
  test "7 returns 'VII'", do: assert Roman.parse(7) == "VII"
  test "8 returns 'VIII'", do: assert Roman.parse(8) == "VIII"
  test "9 returns 'IX'", do: assert Roman.parse(9) == "IX"
  test "23 returns 'XXIII'", do: assert Roman.parse(23) == "XXIII"
  test "94 returns 'XCIV'", do: assert Roman.parse(94) == "XCIV"
  test "914 returns 'XCIV'", do: assert Roman.parse(914) == "CMXIV"
  test "488 returns 'XCIV'", do: assert Roman.parse(488) == "CDLXXXVIII"
end
