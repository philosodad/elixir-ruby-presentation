defmodule Roman do
  @onesarray [:error, "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" ]
  @tensarray [:error, "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" ]
  @hundredsarray [:error, "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" ]

  def parse(num) when num < 10, do: parse_ones(num)

  def parse(num) when num < 100 do
    [head | tail] = break_number num
    parse_tens(head) <> parse(tail_to_int(tail))
  end

  def parse(num) when num < 1000 do
    [head | tail] = break_number num
    parse_hundreds(head) <> parse(tail |> tail_to_int)
  end

  def parse_ones(num),     do: @onesarray     |> Enum.at(num)
  def parse_tens(num),     do: @tensarray     |> Enum.at(num)
  def parse_hundreds(num), do: @hundredsarray |> Enum.at(num)

  defp tail_to_int(tail), do: Enum.join(tail) |> String.to_integer

  defp break_number num do
    Integer.to_string(num) |> 
    String.codepoints |> 
    Enum.map(&(String.to_integer(&1)))
  end
end

